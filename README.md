# FIR_and_FPGAs-review_paper

This paper presents an analysis on the trends for Implementation of FIR filters in FPGAs. Distributed Arithmetic, Multiple Constant Multiplication, Booth and Wallace tree and Subexpression Sharing